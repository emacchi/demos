# about this repo

Repository where I store the demos I do during talks.
They work fine in my environment and I can't guarantee they do in yours.
Fork it, and modify for your needs!

# how to use (e.g. with containers)

You need to run the scripts on Fedora 29.

```
git clone https://gitlab.com/emacchi/demos.git
cd demos/containers
./podman.sh
```

# thanks

https://github.com/containers/Demos has been a great source to create the
scripts. Thanks to the authors!
