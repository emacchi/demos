#!/bin/bash

source prepare
source cleanup
source $demo_magic_dir/demo-magic.sh

podman_rootless() {
    clear

    # Rootless podman
    p "Podman as rootless (impossible with Docker)"
    echo

    pe "id"
    echo

    pe "buildah pull ubi8-minimal"
    echo

    p "non-privileged images (rootless)"
    pe "podman images"
    echo

    p "privileged images (with root)"
    pe "sudo podman images"
    echo

    pe "podman run --rm ubi8-minimal id && echo On the host I am not root && id"
    echo

    p "End of podman rootless demo"
    clear
}

podman_selinux() {
    clear

    # Podman & SElinux
    p "Podman & SElinux demo"
    pe "getenforce"
    echo ""

    p "Run Container with SELinux Breakout"
    echo
    pe "sudo podman run --rm -e HOME=$HOME -ti -v /:/host fedora chroot /host cat /etc/shadow"

    echo
    p "Lets look at the audit log to see what SELinux is reporting"
    echo
    pe "sudo ausearch -m avc -ts recent | grep shadow | tail -1 | grep --color=auto 'system_u:system_r:container_t:s0[^ ]*'"
    echo

    p "End of Podman SElinux demo"
}

podman_rootless
podman_selinux
