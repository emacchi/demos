#!/bin/bash

source prepare
source cleanup
source $demo_magic_dir/demo-magic.sh

buildah_minimal_image() {
    clear

    # Buildah from scratch - minimal images
    p "Buildah from scratch - building minimal images"
    echo ""

    p "sudo buildah from scratch"
    ctr=$(sudo buildah from scratch)
    echo "$ctr"
    echo ""

    p "sudo buildah mount $ctr"
    mnt=$(sudo buildah mount "$ctr")
    echo "$mnt"
    echo ""

    pe "sudo dnf install -y --installroot=$mnt busybox --releasever=29 --disablerepo=* --enablerepo=fedora"
    echo ""

    pe "sudo dnf clean all --installroot=$mnt"
    echo ""

    pe "sudo buildah unmount $ctr"
    echo ""

    pe "sudo buildah commit --rm $ctr minimal-image"
    echo ""

    pe "sudo podman run --rm minimal-image busybox echo Hello world"
    echo ""

    p "End of buildah minimal image demo"
}

buildah_dockerfile() {
    dockerfile="./Dockerfile"
    pythonscript="./HelloFromContainer.py"
    rm -rf $dockerfile pythonscript
    /bin/cat <<- "EOF" > $dockerfile
FROM alpine
RUN apk add python3
ADD HelloFromContainer.py /home
WORKDIR HOME
CMD ["python3","/home/HelloFromContainer.py"]
EOF
    echo
    p "Create image from a Dockerfile and run the container"
    p "Let's look at our Dockerfile"
    echo
    pe "cat $dockerfile"

    echo
    p "Let's look at our Python script"
    echo
    pe "cat $pythonscript"
    /bin/cat <<- "EOF" > $pythonscript
#!/usr/bin/env python3
#
import sys
def main(argv):
    for i in range(0,10):
        print ("Hello World from Container Land! Message # [%d]" % i)
if __name__ == "__main__":
    main(sys.argv[1:])
EOF
    echo

    p "Create the hello image from the Dockerfile"
    echo
    pe "sudo buildah bud -t hello -f $dockerfile ."

    echo
    p "Create the container from the image"
    echo
    pe "sudo buildah from hello"

    echo
    p "Run the container"
    echo
    pe "sudo podman run hello"
}

buildah_minimal_image
buildah_dockerfile
