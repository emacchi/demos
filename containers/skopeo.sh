#!/bin/bash

export DOCKER_ENABLED="1"
source prepare
source cleanup
source $demo_magic_dir/demo-magic.sh

skopeo_inspect() {
    clear
    # Skopeo inspect a remote image
    p "Inspect a remote image using skopeo"
    echo ""

    pe "skopeo inspect docker://docker.io/fedora"
    echo ""

    p "End of skopeo inspect demo"
    clear
}

skopeo_cp_from_docker_to_podman() {
    clear
    p "Copy images from docker storage to podman storage"
    echo ""

    pe "sudo podman images"
    echo ""

    sudo docker pull ubi8-minimal >/dev/null 2>&1
    pe "sudo docker images"
    echo ""

    pe "sudo skopeo copy docker-daemon:ubi8-minimal:latest containers-storage:localhost/ubi8-minimal:demo"
    echo ""

    pe "sudo podman images"
    echo ""

    p "End of Skopeo cp demo"
    clear
}

skopeo_inspect
wait
skopeo_cp_from_docker_to_podman
